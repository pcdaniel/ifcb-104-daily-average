import pandas as pd
import scipy.io as sio
import glob

def calc_datetime(matlab_timestamp):
    """
    Calculate datetime from the matlab timestamp. matlab serial date is represented as the number of days since Jan 1 0000
    To get to a unix epoc, subtract 719529 (ie the number of days between Jan 1 0000 and  Jan 1, 1970).
    https://stackoverflow.com/questions/13965740/converting-matlabs-datenum-format-to-python
    """
    return pd.to_datetime(matlab_timestamp.flatten() - 719529, unit='D')

def load_autoclass_data(fname):
    """
    Load the summary .mat data and format into a pandas dataframe.
    """
    data = sio.loadmat(fname)
    timestamps = calc_datetime(data['mdateTB']) # https://stackoverflow.com/questions/13965740/converting-matlabs-datenum-format-to-python
    classes = [c[0][0] for c in data['class2useTB']]

    df = pd.DataFrame(data=data['classcountTB_above_optthresh'], columns=classes)
    df['dateTime'] = timestamps
    df['vol_analyzed'] = data['ml_analyzedTB']
    df['fnames'] = data['filelistTB']
    return df


def make_dataframe():
    """
    Create a dataframe and appened it for each .mat file (year)
    """
    fnames = sorted(glob.glob('./data/v1_27Aug2019/*.mat'))
    df = pd.DataFrame()
    for fname in fnames:
        print(f"Opening {fname}")
        df = df.append(load_autoclass_data(fname))
    df.index = df['dateTime']

    classes = ['Akashiwo', 'Alexandrium_singlet', 'Amy_Gony_Protoc',
       'Asterionellopsis', 'Centric', 'Ceratium', 'Chaetoceros',
       'Cochlodinium', 'Cryptophyte,NanoP_less10,small_misc', 'Cyl_Nitz',
       'Det_Cer_Lau', 'Dictyocha', 'Dinophysis', 'Eucampia', 'Guin_Dact',
       'Gymnodinium,Peridinium', 'Lingulodinium', 'Pennate', 'Prorocentrum',
       'Pseudo-nitzschia', 'Scrip_Het', 'Skeletonema', 'Thalassionema',
       'Thalassiosira', 'unclassified']
    df_norm = df[classes].divide(df['vol_analyzed'], axis=0)
    return df_norm

def save_daily_values(df_norm):
    """Save daily mean, median, and standard deviation.

    Args:
        df_norm (pd.Datafrane): Pandas dataframe with cell abundance for each of the classes.
    """
    daily_mean = df_norm.resample('1D').mean()
    daily_mean.to_csv("./data/daily_values/ifcb104-daily-mean.csv")
    print("Saving Daily Mean")
    daily_med = df_norm.resample('1D').median()
    daily_med.to_csv("./data/daily_values/ifcb104-daily-median.csv")
    print("Saving Daily Median")
    daily_std = df_norm.resample('1D').std()
    daily_std.to_csv("./data/daily_values/ifcb104-daily-std.csv")
    print(f"Saving Daily SD")


if __name__ == "__main__":
    df = make_dataframe()
    save_daily_values(df)
