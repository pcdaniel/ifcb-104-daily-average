# Daily Auto Classifier Data: IFCB 104 #
Written by P. Daniel on 2022/03/29

## Requirements ##
Pandas, scipy, and glob are all needed to find and read the annual classified data. The script outputs a daily mean, daily median, and daily standard deviation of abundance for each class.

## Running ##
Make sure the .mat files are in the `./data/v1_27Aug2019/` directory. Then the script can be run using:

`python main.py`

## About Code ##
Annual data output from the autoclassifier are stored in .mat (binary matlab) files. These can be accessed using the `scipy.io.load` function.

[x] Load .mat data  
[x] Extract data array: shape(NxM), where N is number of syringes, M is 25, the number of classes used by the model  
[x] Extract the timestamp of each syringe  
[x] Get the sample volume for each syringe: `data['ml_analyzedTB']`  
[x] Loop and append all of the data files  
[x] Normalize each class to the volume to get Abundance  
[x] Calculate daily statistics of cell abdunace  
[x] Write Data to CSV files
